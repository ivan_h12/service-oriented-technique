package model;

public class Ticket {
    private int id;
    private Flight flight;

    public Ticket(){}

    public Ticket(int id, Flight flight) {
        this.id = id;
        this.flight = flight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public String toString() {
        return "Plane: " + flight.getPlaneName() + "\n" +
                "Trip: " + flight.getOrigin() + " - " + flight.getOrigin() + "\n" +
                "Date: " + flight.getDate() + "\n" +
                "Time: " + flight.getTime() + "\n" +
                "Price: " + flight.getPrice() + "\n";
    }
}
