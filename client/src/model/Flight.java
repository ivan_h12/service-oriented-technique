package model;

public class Flight {
    private int id;
    private String planeName, destination, origin, date, time, price;

    public Flight(){}

    public Flight(int id, String planeName, String origin, String destination, String date, String time, String price) {
        this.id = id;
        this.planeName = planeName;
        this.destination = destination;
        this.origin = origin;
        this.date = date;
        this.time = time;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaneName() {
        return planeName;
    }

    public void setPlaneName(String planeName) {
        this.planeName = planeName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Plane: " + planeName + "\n" +
                "Trip: " + origin + " - " + destination + "\n" +
                "Date: " + date + "\n" +
                "Time: " + time + "\n" +
                "Price: " + price;
    }
}
