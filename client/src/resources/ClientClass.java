package resources;

import model.Flight;
import model.Ticket;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ClientClass {
    private static Invocation.Builder requestBuilder;
    private static Response response;
    private static WebTarget serviceTarget;

    private static void printError(Response response){
        String errorMessage = response.readEntity(String.class);
        System.out.println("ERROR: " + errorMessage + "\n");
    }

    public static void getAllFlight(){
        requestBuilder = serviceTarget.path("allflights").request().accept(MediaType.APPLICATION_JSON);
        response = requestBuilder.get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
            GenericType<ArrayList<Flight>> genericType = new GenericType<>(){};
            ArrayList<Flight> entity = response.readEntity(genericType);
            int count = 1;
            for(Flight flight : entity){
                System.out.print("\n" + count + ". "); count++;
                System.out.println(flight.toString());
            }
        } else {
            printError(response);
        }
    }

    public static void getFlightById(int idFlight){
        String path = String.valueOf(idFlight);
        requestBuilder = serviceTarget.path(path).request().accept(MediaType.APPLICATION_JSON);
        response = requestBuilder.get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
           Flight flight = response.readEntity(Flight.class);
           System.out.print("\n" + idFlight + ". ");
            System.out.println(flight.toString());
        } else {
            printError(response);
        }
    }

    public static List<Flight> getFlightByOriginDestination(String origin, String dest){
        requestBuilder = serviceTarget.queryParam("origin", origin).queryParam("destination", dest).request().accept(MediaType.APPLICATION_JSON);
        response = requestBuilder.get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
            GenericType<ArrayList<Flight>> genericType = new GenericType<>(){};
            ArrayList<Flight> entity = response.readEntity(genericType);
            int count = 1;
            for(Flight flight : entity){
                System.out.println("\n" + count + ". " + origin.toUpperCase() + " to " + dest.toUpperCase()); count++;
                System.out.println(flight.toString());
            }
            if(entity.size() > 0) return entity;
        } else {
            printError(response);
        }
        return null;
    }

    public static void bookingFlight(Flight flight){
        Entity<Flight> entity = Entity.entity(flight, MediaType.APPLICATION_JSON);
        response = serviceTarget.path("userticket").request().accept(MediaType.TEXT_PLAIN).post(entity);
        if(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()){
            System.out.println("Booking Success!\n");
        }else{
            printError(response);
        }
    }

    public static List<Ticket> getAllUserTicket(){
        requestBuilder = serviceTarget.path("userticket/all").request().accept(MediaType.APPLICATION_JSON);
        response = requestBuilder.get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
            GenericType<ArrayList<Ticket>> genericType = new GenericType<>(){};
            ArrayList<Ticket> entity = response.readEntity(genericType);
            int count = 1;
            for(Ticket ticket : entity){
                System.out.print("\n" + count + ". "); count++;
                System.out.println(ticket.toString());
            }
            if(entity.size() > 0) return entity;
        } else {
            printError(response);
        }
        return null;
    }

    public static void cancelBooking(int id){
        WebTarget resourceTarget = serviceTarget.path("userticket/" + String.valueOf(id));
        requestBuilder = resourceTarget.request().accept(MediaType.TEXT_PLAIN);
        response = requestBuilder.delete();
        if(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()){
            System.out.println("Booking canceled\n");
        }else{
            printError(response);
        }
    }

    public static void main(String[] args) {
        int menu = -1;
        String confirmation;

        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        //URI baseURI = UriBuilder.fromUri("http://localhost:8001/plane/rest/flights").build();
        URI baseURI = UriBuilder.fromUri("http://localhost:9090/flights").build();
        serviceTarget = client.target(baseURI);

        while(menu != 0){
            System.out.println("===== MENU =====");
            System.out.println("1. Check all flights");
            System.out.println("2. View flight by ID");
            System.out.println("3. Search flight by origin and destination");
            System.out.println("4. See All Flight Tickets");
            System.out.println("5. Cancel Booking");
            System.out.println("0. Exit");
            System.out.print("Choice: ");
            Scanner input = new Scanner(System.in);
            menu = Integer.parseInt(input.nextLine());
            if(menu == 1){
                getAllFlight();
            }else if(menu == 2){
                System.out.print("Insert flight ID: ");
                int idFlight = input.nextInt();
                getFlightById(idFlight);
            }else if(menu == 3){
                String origin, dest;
                int booking;
                System.out.print("Insert your origin: ");
                origin = input.nextLine();
                System.out.print("Insert your destination: ");
                dest = input.nextLine();
                List<Flight> list = getFlightByOriginDestination(origin, dest);
                if(!list.isEmpty()){
                    System.out.print("Do you want to book flight (y/n): ");
                    confirmation = input.nextLine();
                    if(confirmation.equalsIgnoreCase("y")){
                        System.out.print("Choose the flight you want (by number): ");
                        booking = Integer.parseInt(input.nextLine());
                        bookingFlight(list.get(booking - 1));
                    }
                }
            }else if(menu == 4){
                getAllUserTicket();
            }else if(menu == 5){
                List<Ticket> usersBooking = getAllUserTicket();
                if(usersBooking != null){
                    System.out.print("Do you want to cancel your booking (y/n): ");
                    confirmation = input.nextLine();
                    if(confirmation.equalsIgnoreCase("y")){
                        System.out.print("Choose a ticket that you want to cancel (by number): ");
                        int num = Integer.parseInt(input.nextLine());
                        if(num - 1 < usersBooking.size())
                            cancelBooking(usersBooking.get(num - 1).getId());
                        else System.out.println("You only have " + usersBooking.size() + " tickets!\n");
                    }
                }
            }
        }
    }
}
