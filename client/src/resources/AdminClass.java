package resources;

import model.Flight;
import model.Ticket;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class AdminClass {
    private static Invocation.Builder requestBuilder;
    private static Response response;
    private static WebTarget serviceTarget;

    private static void printError(Response response){
        String errorMessage = response.readEntity(String.class);
        System.out.println("ERROR: " + errorMessage + "\n");
    }

    public static List<Flight> getAllFlight(){
        requestBuilder = serviceTarget.path("allflights").request().accept(MediaType.APPLICATION_JSON);
        response = requestBuilder.get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
            GenericType<ArrayList<Flight>> genericType = new GenericType<>(){};
            ArrayList<Flight> entity = response.readEntity(genericType);
            int count = 1;
            for(Flight flight : entity){
                System.out.print("\n" + count + ". "); count++;
                System.out.println(flight.toString());
            }
            if(entity.size() > 0) return entity;
        } else {
            printError(response);
        }
        return null;
    }

    public static void getFlightById(int idFlight){
        String path = String.valueOf(idFlight);
        requestBuilder = serviceTarget.path(path).request().accept(MediaType.APPLICATION_JSON);
        response = requestBuilder.get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
            Flight flight = response.readEntity(Flight.class);
            System.out.print("\n" + idFlight + ". ");
            System.out.println(flight.toString());
        } else {
            printError(response);
        }
    }

    public static void addFlight(String planeName, String origin, String destination, String date, String time, String price){
        Form form = new Form();
        form.param("planeName", planeName);
        form.param("origin", origin);
        form.param("destination", destination);
        form.param("date", date);
        form.param("time", time);
        form.param("price", price);
        Entity<Form> entity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED);

        response = serviceTarget.request().accept(MediaType.TEXT_PLAIN).post(entity);
        if(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()){
            System.out.println("New Flight Added");
        }else{
            printError(response);
        }
    }

    public static void updateFlight(int id, String planeName, String origin, String destination, String date, String time, String price){
        Flight flight = new Flight(id, planeName, origin, destination, date, time, price);
        Entity<Flight> entity = Entity.entity(flight, MediaType.APPLICATION_JSON);

        response = serviceTarget.request().accept(MediaType.TEXT_PLAIN).put(entity);
        if(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()){
            System.out.println("Update flight id " + id + " success");
        }else{
            printError(response);
        }
    }

    public static void deleteFlight(int id){
        WebTarget resourceTarget = serviceTarget.path(String.valueOf(id));
        requestBuilder = resourceTarget.request().accept(MediaType.TEXT_PLAIN);
        response = requestBuilder.delete();
        if(response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()){
            System.out.println("Deleted flight with id " + id + " success");
        }else {
            printError(response);
        }
    }

    public static void main(String[] args) {
        int menu = -1;

        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        //URI baseURI = UriBuilder.fromUri("http://localhost:8001/plane/rest/flights").build();
        URI baseURI = UriBuilder.fromUri("http://localhost:9090/flights").build();
        serviceTarget = client.target(baseURI);

        while(menu != 0){
            System.out.println("===== MENU =====");
            System.out.println("1. Create new flight");
            System.out.println("2. Update flight");
            System.out.println("3. Delete flight");
            System.out.println("4. See all Flight");
            System.out.println("5. See flight by ID");
            System.out.println("0. Exit");
            System.out.print("Choice: ");
            Scanner input = new Scanner(System.in);
            menu = Integer.parseInt(input.nextLine());
            if(menu == 1){
                String planeName, origin, destination, date, time, price;
                System.out.print("Insert plane name: ");
                planeName = input.nextLine();
                System.out.print("Insert origin: ");
                origin = input.nextLine();
                System.out.print("Insert destination: ");
                destination = input.nextLine();
                System.out.print("Insert date: ");
                date = input.nextLine();
                System.out.print("Insert time: ");
                time = input.nextLine();
                System.out.print("Insert price: ");
                price = input.nextLine();
                addFlight(planeName, origin, destination, date, time, price);
            }else if(menu == 2){
                List<Flight> flightList = getAllFlight();
                if (flightList != null){
                    System.out.print("Choose flight to update (by number): ");
                    int num = Integer.parseInt(input.nextLine());
                    if(num - 1 < flightList.size()){
                        String planeName, origin, destination, date, time, price;
                        System.out.print("Insert plane name: ");
                        planeName = input.nextLine();
                        System.out.print("Insert origin: ");
                        origin = input.nextLine();
                        System.out.print("Insert destination: ");
                        destination = input.nextLine();
                        System.out.print("Insert date: ");
                        date = input.nextLine();
                        System.out.print("Insert time: ");
                        time = input.nextLine();
                        System.out.print("Insert price: ");
                        price = input.nextLine();
                        updateFlight(flightList.get(num - 1).getId(), planeName, origin, destination, date, time, price);
                    }
                    else System.out.println("ERROR: Cannot find flight with id " + flightList.get(num - 1).getId() + "\n");
                }
            }else if(menu == 3){
                List<Flight> flightList = getAllFlight();
                if (flightList != null){
                    System.out.print("Do you want to delete a flight (y/n): ");
                    String confirmation = input.nextLine();
                    if(confirmation.equalsIgnoreCase("y")){
                        System.out.print("Choose a flight that you want to delete (by number): ");
                        int num = Integer.parseInt(input.nextLine());
                        if(num - 1 < flightList.size())
                            deleteFlight(flightList.get(num - 1).getId());
                        else System.out.println("ERROR: Cannot find flight with id " + flightList.get(num - 1).getId() + "\n");
                    }
                }
            }else if(menu == 4) {
                getAllFlight();
            }else if(menu == 5){
                int id = Integer.parseInt(input.nextLine());
                getFlightById(id);
            }
        }
    }
}
