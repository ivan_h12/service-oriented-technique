package resources;

import model.Flight;
import model.Ticket;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("flights")
@Singleton
public class FlightResources {

    private List<Flight> flightList = new ArrayList<>();
    private static List<Ticket> usersBooking = new ArrayList<>();

    public FlightResources(){
        flightList.add(new Flight(1, "British Airways","Netherlands", "United Kingdom", "01-01-2020", "09:00", "$100" ));
        flightList.add(new Flight(2, "Garuda","Indonesia", "Singapore", "21-11-2020", "21:00", "$100" ));
        flightList.add(new Flight(3, "British Airways","Netherlands", "United Kingdom", "08-04-2020", "13:00", "$100" ));
        flightList.add(new Flight(4, "Garuda","Indonesia", "Singapore", "22-11-2020", "00:00", "$100" ));
    }

    @GET
    @Path("flight")
    @Produces(MediaType.APPLICATION_JSON)
    public Flight getFlightSearched(){
        if(flightList.size() > 0)
            return flightList.get(0);
        else
            throw new RuntimeException("There are no flight");
    }

    @GET //GET at http://localhost:XXXX/flights/allflights
    @Path("allflights")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFlights(){
        GenericEntity<List<Flight>> entity = new GenericEntity<>(flightList){};
        return Response.ok(entity).build();
    }

    @GET //GET at http://localhost:XXXX/flights/1
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getFlightById(@PathParam("id") int idFlight){
        for(Flight flight : flightList){
            if(flight.getId() == idFlight){
                return Response.ok(flight).build();
            }
        }
        return Response.serverError().entity("Cannot find flight with id " + idFlight).build();
    }

    @GET //GET at http://localhost:XXXX/flights?origin=origin&destination=dest
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFlightByDestOrigin(@QueryParam("origin") String origin, @QueryParam("destination") String dest){
        List<Flight> entity = new ArrayList<>();
        for(Flight flight : flightList){
            if(flight.getOrigin().equalsIgnoreCase(origin) && flight.getDestination().equalsIgnoreCase(dest)){
                entity.add(flight);
            }
        }
        if(entity.isEmpty()){
            return Response.serverError().entity("Cannot find flight from " + origin + " to " + dest).build();
        }
        return Response.ok(entity).build();
    }

    @POST //POST at http://localhost:XXXX/flights/userticket
    @Path("userticket")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response bookingFlight(Flight flight){
        int maxId = getTicketMaxId();
        Ticket ticket = new Ticket(maxId + 1, flight);
        usersBooking.add(ticket);
        return Response.noContent().build();
    }

    @GET //GET at http://localhost:XXXX/flights/userticket/all
    @Path("userticket/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUserTicket(){
        GenericEntity<List<Ticket>> entity = new GenericEntity<>(usersBooking){};
        if(!usersBooking.isEmpty()){
            return Response.ok(entity).build();
        }
        return Response.serverError().entity("No ticket yet!").build();
    }

    @DELETE //DELETE at http://localhost:XXXX/flights/userticket/1
    @Path("userticket/{id}")
    public Response cancelBooking(@PathParam("id") int id){
        Ticket ticket = this.getTicket(id);
        if(ticket != null){
            usersBooking.remove(ticket);
            return Response.noContent().build();
        }
        return Response.serverError().entity("Cannot find ticket with id " + id).build();
    }

    @POST //POST at http://localhost:XXXX/flights
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addFlight(@FormParam("planeName") String planeName, @FormParam("origin") String origin, @FormParam("destination") String destination,
                          @FormParam("date") String date, @FormParam("time") String time, @FormParam("price") String price){
        int maxId = getFlightMaxId();
        Flight flight = new Flight(maxId + 1, planeName, origin, destination, date, time, price);
        flightList.add(flight);
        return Response.noContent().build();
    }

    @PUT //PUT at http://localhost:XXXX/flights
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFlight(Flight flight){
        Flight existingFlight = this.getFlight(flight.getId());
        System.out.println(existingFlight);
        if(existingFlight != null){
            existingFlight.setPlaneName(flight.getPlaneName());
            existingFlight.setOrigin(flight.getOrigin());
            existingFlight.setDestination(flight.getDestination());
            existingFlight.setDate(flight.getDate());
            existingFlight.setTime(flight.getTime());
            existingFlight.setPrice(flight.getPrice());
            return Response.noContent().build();
        }else{
            return Response.serverError().entity("Cannot find flight with id " + flight.getId()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteFlight(@PathParam("id") int id){
        Flight flight = this.getFlight(id);
        if(flight != null){
            flightList.remove(flight);
            return Response.noContent().build();
        }
        return Response.serverError().entity("Cannot find flight with id " + id).build();
    }

    public Ticket getTicket(int id){
        for(Ticket ticket : usersBooking){
            if(ticket.getId() == id){
                return ticket;
            }
        }
        return null;
    }

    public int getTicketMaxId(){
        int maxId = -1;
        for(Ticket ticket : usersBooking ){
            int currId = ticket.getId();
            if(currId > maxId) maxId = currId;
        }
        return maxId;
    }

    public int getFlightMaxId(){
        int maxId = -1;
        for(Flight flight : flightList ){
            int currId = flight.getId();
            if(currId > maxId) maxId = currId;
        }
        return maxId;
    }

    public Flight getFlight(int id){
        for(Flight flight : flightList){
            if (flight.getId() == id) return flight;
        }
        return null;
    }


}
